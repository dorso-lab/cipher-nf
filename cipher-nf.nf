params.genomeSize = 'hs'
params.threads = 1
params.rpgc = 2451960000
params.fragmentSize = 150
params.genome = 'hg19'
params.epicfs = 150

// print help usage
if (params.help) {
	log.info ''
	log.info '=============================='
	log.info '         C I P H E R          '
	log.info '=============================='
	log.info 'A ChIP-seq processing pipeline'
	log.info ''
	log.info 'Run: '
	log.info '    cipher-nf.nf --config --index --genomeSize --genome [OPTIONS]...'
	log.info ''
	log.info 'General Options: '
	log.info '    --help               Show this message and exit.'
	log.info '    --config             A TAB seperated file containing metadata information.'
	log.info '    --index              An appropriate index or genome fasta file for chosen aligner.'
	log.info '    --threads            The number of threads cipher should use. (Default: 1)'
	log.info ''
	log.info 'Mapping Options: '
	log.info '    --bowtie2            Use the Bowtie2 short read aligner for mapping.'
	log.info '    --bbmap              Use the BBmap short read aligner for mapping.'
	log.info ''
	log.info 'BigWig Options: '
	log.info '    --rpgc               RPGC normalization uses the effective genome size of a species. (Default: human)'
	log.info '    --fragmentSize       Extends reads to fragment size. (Default: 150)'
	log.info ''
	log.info 'MACS2 Options: '
	log.info '    --genomeSize         The genome size of your reference species as indicated in the MACS2 manual. (Default: hs)'
	log.info ''
	log.info 'Epic Options: '
	log.info '    --genome             The genome to analyze as indicated in Epic manual. (Default: hg19)'
	log.info '    --epicfs             The size of the sequenced fragment. (Default: 150)'
	log.info ''
	log.info 'E-mail Options: '
	log.info '    --email              Set this option to recieve an e-mail when your run completes or errors out.'
	log.info '    --address            The email address that you would like to recieve run information on.'
	log.info ''
	exit 1
}

config = file(params.config)

// Check input parameters
if (!params.config) {
	exit 1, "Please specify a config file"
}

if (!params.index) {
    exit 1, "Please specify an index file"
}

// End of input parameter check

// Open up a fastq channel
fastqs = Channel
.from(config.readLines())
.map { line ->
	list = line.split()
	sampleID = list[0]
	path = list[1]
	controlID = list[2]
	mark = list[3]
	[ sampleID, path, controlID, mark ]
}

// Step 1. Map to reference genome using chosen aligner

process mapping {

    publishDir "/home/carlos/Dropbox/testing/Alignments", mode: 'copy'

	input:
	set prefix, file(fastq), controlID, mark from fastqs

	output:
	set prefix, file("${prefix}.sorted.bam"), controlID, mark into bams

	script:
	if( params.bowtie2 )
	"""
	bowtie2 -q --very-sensitive-local -p ${params.threads} -x ${params.index} -U ${fastq} | samtools view -@ ${params.threads} -b -F3844 - | samtools sort -@ ${params.threads} -O bam -o ${prefix}.sorted.bam
	"""

	else if( params.bbmap )
	"""
	bbmap.sh in=${fastq} out=stdout.sam ref=${params.index} minid=.95 local=t | samtools view -@ ${params.threads} -b -F3844 - | samtools sort -@ ${params.threads} -o ${prefix}.sorted.bam
	"""

    else
        error "No aligner chosen"
}

// copy bams channel to several others

bams.into {
	bams1
	bams2
	bams3
	bams4
	bams5
}

// Step 2. Index sorted BAM files

process bam_index {

	publishDir '/home/carlos/Dropbox/testing/Alignments', mode: 'copy'

	input:
	set prefix, file(bam), controlID, mark from bams1

	output:
	set prefix, file("${prefix}.sorted.bam.bai"), controlID, mark into bamIndex

	script:
	"""
	samtools index ${bam}
	"""
}

// Step 3. Create bigWig files

process create_bigWig {

    publishDir '/home/carlos/Dropbox/testing/Tracks', mode: 'copy'

    input:
    set prefix, file(bam), controlID, mark from bams2

    output:
    set prefix, file("${prefix}.bigWig"), controlID, mark into bigwigs

    script:
    """
    bamCoverage -b ${bam} -o ${prefix}.bigWig -bs 50 -p ${params.threads} --normalizeTo1x ${params.rpgc} --extendReads ${params.fragmentSize}
	"""
}

// Step 4. Create bedGraph files

process create_bedGraph {

	publishDir '/home/carlos/Dropbox/testing/Tracks', mode: 'copy'

	input:
	set prefix, file(bam), controlID, mark from bams3

	output:
	set prefix, file("${prefix}.bedGraph"), controlID, mark into bedgraphs

	script:
	"""
	bamCoverage -b ${bam} -o ${prefix}.bedGraph -of=bedgraph -bs 50 -p ${params.threads}
	"""
}

// Separate bams and inputs

treat = Channel.create()
control = Channel.create()
bams4.choice(treat, control) {
	it[3] == 'input' ? 1 : 0
}

// Grab BAMs with no control

bams5.tap { allBams }
.filter {
	it[2] == '-'
}.map {
	[it[0], it[1], it[3]]
}.tap { bamsNoInput }

// Grab BAMs with controls

bamsWithInput = control.filter {
	it[2] != '-'
}
.cross(allBams) { it[1] }.map { c, t ->
	[t[0], t[1], c[1], t[3]]
}

// Step 5. MACS2 peak calling with input

process call_narrowpeaks_input {

	publishDir '/home/carlos/Dropbox/testing/Peaks', mode: 'copy'

	input:
	set prefix, file(bam), file(control), mark from bamsWithInput

	output:
	set prefix, file("${prefix}_peaks.narrowPeak"), mark, val("narrowPeak") into narrowPeakInput, bamsWithInput1

	script:
	"""
	macs2 callpeak -t ${bam} -c ${control} -n ${prefix} --outdir Peaks -f BAM -g ${params.genomeSize} -p 1e-2
	"""
}

// Step 6. MACS2 peak calling no input

process call_narrowpeaks_no_input {

	publishDir '/home/carlos/Dropbox/testing/Peaks', mode: 'copy'

	input:
	set prefix, file(bam), mark from bamsNoInput

	output:
	set prefix, file("${prefix}_peaks.narrowPeak"), mark, val("narrowPeak") into narrowPeakNoInput

	script:
	"""
	macs2 callpeak -t ${bam} -n ${prefix} --outdir Peaks -f BAM -g ${params.genomeSize} -p 1e-2
	"""
}

// Step 7. Epic broad peak calling with input

bamsWithInput1.into {
	bamsWithInput_epic
}

process call_broadpeaks_input {

	publishDir '/home/carlos/Dropbox/testing/Peaks', mode: 'copy'

	input:
	set prefix, file(bam), file(control), mark from bamsWithInput_epic

	output:
	set prefix, file("${prefix}_epic.bed"), mark, val("broadPeak") into broadPeakInput

	script:
	"""
	epic --treatment ${bam} --control ${control} --number-cores ${params.threads} --genome ${params.genome} --fragment-size ${params.epicfs}
	"""
}

if (params.email) {

    workflow.onComplete {
        def subject = 'Pipeline Execution'
        def recipient = "${params.address}"
        ['mail', '-s', subject, recipient].execute() << """

        Pipeline Execution Summary
        --------------------------
        Completed at:   ${workflow.complete}
        Duration    :   ${workflow.duration}
        Success     :   ${workflow.success}
        workDir     :   ${workflow.workDir}
        exit status :   ${workflow.exitStatus}
        Error report:   ${workflow.errorReport ?: '-'}
        """
	}
}
